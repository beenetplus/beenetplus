# BeeNetPLUS

The BeeNetPLUS workflow is an end-to-end single cell RNAseq workflow designed to process data from **Honeycomb Biotechnologies' HIVE<sup>TM</sup> scRNA Solution**. This workflow combines BeeNet software along with preliminary downstream scRNA-seq analysis to provide the customer with a basic understanding of their data. The BeeNetPLUS workflow is only available on Terra.bio and can only be used for HIVE scRNAseq data. 
The BeeNetPLUS workflow is an end-to-end single cell RNAseq workflow designed to process data from **[Honeycomb Biotechnologies' HIVE<sup>TM</sup> scRNA Solution](https://honeycomb.bio/the-hive/)**. This workflow combines BeeNet software along with preliminary downstream scRNA-seq analysis to provide the customer with a basic understanding of their data. The BeeNetPLUS workflow is only available on Terra.bio and can only be used for HIVE scRNAseq data. 


BeeNet<sup>TM</sup> is a custom software designed to process data from paired-end Illumina® sequencing of single-cell RNA-seq libraries produced by the HIVE<sup>TM</sup> scRNAseq Processing Kit. The software consists of a set of programs that receives demultiplexed FASTQ file inputs and yields a transcript and gene count matrix (CM), aligned BAM file, and a quality metric (QC) file.

The Terra.bio BeeNetPLUS pipeline performs preliminary scRNAseq analysis using a Seurat-based workflow. Count matrices are input into the workflow, which performs basic filtering, clustering, cell-type annotation, and marker gene analysis. A basic analysis report is generated at the end of this workflow that provides quality metrics, plots, and an R object for additional custom analysis.

More information about the BeeNetPLUS workflow can be found on the [Honeycomb Biotechnologies Support Page](https://honeycombbio.zendesk.com/hc/en-us).

BeeNet<sup>TM</sup> is also available as an executable file that can be run only on Linux<sup>TM</sup> systems. The [BeeNet<sup>TM</sup> Software User Guide](https://honeycombbio.zendesk.com/hc/en-us/articles/4408694864283-BeeNet-v1-1-X-Software-Guide) along with the accompanying video tutorials [BeeNet<sup>TM</sup> Download](https://honeycombbio.zendesk.com/hc/en-us/articles/4408704149147-BeeNet-v1-1-X-Installation-Video) and [BeeNet<sup>TM</sup> Running Analysis](https://honeycombbio.zendesk.com/hc/en-us/articles/4408704170907-BeeNet-v1-1-X-Analysis-Video), detail how to download, install, and use the software to analyze HIVE scRNAseq data on a Linux<sup>TM</sup> system from the command-line. BeeNetPLUS is not available as a downloadable executable file.
